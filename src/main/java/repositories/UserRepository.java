package repositories;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.User;

@Stateless
@Named
public class UserRepository {

	@PersistenceContext(unitName = "TestPersistence")
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<User> getUsers() {
		List<User> users = new ArrayList<>();
		Query query = entityManager.createQuery("select u from User u");
		users = query.getResultList();
		return users;
	}

	/*
	 * public List<Team> getTeams(){ return
	 * entityManager.createQuery("Select t from Team t").getResultList();
	 * 
	 * }
	 */

}
