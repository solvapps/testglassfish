package managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


import entities.User;
import repositories.UserRepository;


//import repositories.EmployeeRepository;

//import repositories.EmployeeRepository;


//@ManagedBean(name = "helloWorld", eager = true)
@Named("helloWorld")
@RequestScoped
public class HelloWorld implements Serializable{
	
	@Inject
	UserRepository userRepository;
   

   public HelloWorld() {
   }
	
   public String getMessage() {
	   System.out.println("HelloWorld started! " + userRepository.getUsers().size()/* empl.getCount()*/);
	   List<User> users = userRepository.getUsers();
	   return "size : " + users.size();
   }
}