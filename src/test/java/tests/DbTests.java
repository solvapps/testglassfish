package tests;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;

import entities.User;
import junit.framework.Assert;
import repositories.UserRepository;

import static org.mockito.Mockito.*;


public class DbTests {

	
	UserRepository userRepository;
	
	@Before
	public void init() {
		
		userRepository = new UserRepository();
		userRepository.setEntityManager(mock(EntityManager.class));
		when(userRepository.getUsers()).thenReturn(new ArrayList<User>());

	 } 
	
	@Test
	public void myTest() {
		
		
		//UserRepository userRepository = new UserRepository();
		
		//List<User> u = userRepository.getUsers();
		
		//System.out.println(u.size());
		
		Assert.assertEquals(1, 1);
		
		
		
	}
	
}
