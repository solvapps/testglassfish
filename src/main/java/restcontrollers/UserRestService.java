package restcontrollers;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import entities.User;
import repositories.UserRepository;

@Path("/userlist")
@Named
@Stateless
public class UserRestService {

	@Inject
	UserRepository userrepo;
	
	@GET
	@Path("{id}")
	public Response getUserById(@PathParam("id") String id) {
		
		List<User> liste = userrepo.getUsers();
		String response = liste.get(0).getName();
		
		//String response = "";
		
	   return Response.status(200).entity("getUserById is called, id : " + id + " - > " + response).build();
	}

}
