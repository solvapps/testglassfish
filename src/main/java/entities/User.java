package entities;


import entities.Team;


import javax.persistence.*;

/*
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "getFirst",
                query = "select * from world.user limit 1",
                resultClass=User.class
        )
})
*/

@Entity
@Table(name = "user")
public class User {

    @Id
    private Long id;

    @Column(nullable = true,length = 1000)
    private String name;
    private String surname;

    @ManyToOne
    private Team team;



    public User(Long id, String name, String surname, Long teamid) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.team = new Team(teamid,"");
    }

    public User(){

    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


}
